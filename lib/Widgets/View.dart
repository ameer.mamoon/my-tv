// ignore_for_file: file_names

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:my_tv/API/Controllers/ShowController.dart';
import 'package:my_tv/API/models/tvModels.dart';
import 'package:my_tv/Screens/default.dart';

class HomeTVWidget extends StatelessWidget {
  final tvModel model;


  HomeTVWidget(this.model);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double length = min(size.height, size.width);

    return GestureDetector(
      child: Container(
        width: length*.45,
        height: length*.75,
        margin: EdgeInsets.all(length*.025),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(length*0.05),
          color: Theme.of(context).cardColor,

        ),

        child: Column(
          children: [
            Expanded(
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(model.image),
                  ),
                  borderRadius: BorderRadius.vertical(top: Radius.circular(length*0.05)),
              ),

            ),
          ),
            ListTile(
              title: Text(model.title),
              subtitle: Row(
                children: [
                  Icon(Icons.star,color: Colors.amber,),
                  Text('${model.imDbRating}/10'),
                ],
              ),
              trailing: Text(model.rank),
            )
          ],
        ),
      ),
      onTap: (){
        Navigator.push(context, MaterialPageRoute(

            builder: (context){

                return ShowInfoScreen(model);

              }

        ));

      },

    );
  }

  static Future<List<Widget>> getTop250TVs() async {
    List<tvModel> models = await ShowController().getTop250TVs();

    List<Widget> widgets = [];
    for(var x in models){
      widgets.add(HomeTVWidget(x));
    }
    return widgets;
  }

  static Future<List<Widget>> getTop250Movies() async {

    List<tvModel> models = await ShowController().getTop250Movies();

    List<Widget> widgets = [];
    for(var x in models){
      widgets.add(HomeTVWidget(x));
    }

    return widgets;
  }

}

class SearchResultView extends StatelessWidget {
  final String value;

  SearchResultView(this.value);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: ShowController().search(value),
      builder: (context,snapShot){
        if(snapShot.hasData){
          List<searchModel> models = (snapShot.data as List<searchModel>);
          if(models.isEmpty) {
            return const Center(
              child: Text('No results!'),
            );
          }

          return ListView.builder(
              itemCount: models.length,
              itemBuilder: (context,i){
                return Card(
                  child: ListTile(
                    title: Text(models[i].title),
                    subtitle: Text(models[i].description),
                    leading: CircleAvatar(
                      foregroundImage: NetworkImage(models[i].image),
                    ),
                  ),
                );
              }
              );
        }
        else if(snapShot.hasError){
          return Center(
            child: Text('Error : ${snapShot.error}'),
          );
        }
        return const Center(
          child: CircularProgressIndicator()
        );
      },
    );
  }
}

