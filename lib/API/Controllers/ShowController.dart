// ignore_for_file: file_names

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:my_tv/API/models/tvModels.dart';


class ShowController{

  static final ShowController _controller = ShowController._();

  ShowController._();

   factory ShowController(){
    return _controller;
  }

   Future<List<tvModel>> getTop250TVs() async {
    var response = await http.get(Uri.parse('https://jsonkeeper.com/b/O2NC'));//https://imdb-api.com/en/API/Top250TVs/k_c5mb66ix
    Map<String,dynamic> data = jsonDecode(response.body);

    List<dynamic> items = data['items'];
    List<tvModel> models = [];
    for(var x in items){
      models.add(tvModel.map(x));
    }

    return models;
  }

   Future<List<tvModel>> getTop250Movies() async {
    var response = await http.get(Uri.parse('https://jsonkeeper.com/b/L6JX'));//https://imdb-api.com/en/API/Top250moviess/k_c5mb66ix
    Map<String,dynamic> data = jsonDecode(response.body);

    List<dynamic> items = data['items'];
    List<tvModel> models = [];
    for(var x in items){
      models.add(tvModel.map(x));
    }
    print(models.length);
    return models;
  }

   Future<List<searchModel>>  search(String value) async {
     var response = await http.get(Uri.parse('https://imdb-api.com/en/API/SearchTitle/k_2k6w9uxj/$value'));
     Map<String,dynamic> data = jsonDecode(response.body);
     List<searchModel> models = [];
     List<dynamic> results = data['results'];

     for(var x in results){
       models.add(searchModel.map(x));
     }

     return models;
   }

}