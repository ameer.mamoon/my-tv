// ignore_for_file: file_names

import 'dart:math';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_tv/API/models/tvModels.dart';

class ShowWidgetsAPI extends StatelessWidget {
  final Future<Object> future;

  ShowWidgetsAPI(this.future);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: future,
        builder: (context, asyncSnapShot) {
          if (asyncSnapShot.hasData) {
            return SingleChildScrollView(
              child: Wrap(
                children: asyncSnapShot.data as List<Widget>,
              )
            );
          }

          else if (asyncSnapShot.hasError) {
            return  const Center(
              child:Text('No Internet Connection'),
            );
          }


          return const Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}

class ShowInfoWidget extends StatelessWidget {
  final tvModel model;

  ShowInfoWidget(this.model);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double length = min(size.height,size.width);
    return Container(

      height: double.infinity,
      width: double.infinity,
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.only(top:length*0.05),
      child: LayoutBuilder(
        builder: (context,constrains) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                    child: FittedBox(
                      child: Text(model.fullTitle,style: TextStyle(fontWeight: FontWeight.bold,),
                      ),
                      fit: BoxFit.fitWidth,
                    ),
                  padding: EdgeInsets.only(bottom: constrains.maxHeight*.05),
                  width: constrains.maxWidth*.8,
                ),
                FittedBox(
                  alignment: Alignment.centerLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      detailsWidget('IMDB rating : ', '${model.imDbRating}/10',widget: Icon(Icons.star,color: Colors.amber,)),
                      detailsWidget('total rates : ', model.imDbRatingCount,),
                      detailsWidget('world rank : ', model.rank,),
                      detailsWidget('year : ', model.year,),
                      detailsWidget('crew : ', model.crew,),
                    ],
                  ),
                )
              ],
            ),
          );
        }
      ),
    );
  }
}

class detailsWidget extends StatelessWidget {
  final String title,content;
  final double factor;
  final Widget? widget;
  detailsWidget(this.title, this.content,{this.factor = .03,this.widget});

  @override
  Widget build(BuildContext context) {
    return
      Container(
        child: FittedBox(

          child: Row(
            children: [
              Text(title),
              Text(
                content,
                style: TextStyle(fontWeight: FontWeight.bold,),
                
              ),
              widget ?? SizedBox()
            ],
          ),
        ),

      );
  }
}

class SearchController extends GetxController{

  bool isSearch = false;

  void press(){
    isSearch = !isSearch;
    update();
  }


}

class SearchBar extends StatelessWidget {
  final String title;
  final SearchController controller= Get.put(SearchController());
  final TextEditingController textController;

  SearchBar({required this.title,required this.textController});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchController>(
      init: controller,
        builder: (controller){
          return controller.isSearch ?
          TextField(
            controller: this.textController,
            decoration:InputDecoration(
              hintText: 'Search a movie or TV show',
              icon: IconButton(
                  icon:const Icon(Icons.close),
                  onPressed: (){
                    textController.text = '';
                    controller.press();
                  },
              )
            ),
          ):
          Text(title);
        }
    );
  }
}
