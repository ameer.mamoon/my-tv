import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_tv/API/models/tvModels.dart';
import 'package:my_tv/Screens/Elements.dart';
import 'package:my_tv/Widgets/View.dart';

class DefaultScreen extends StatelessWidget {
  final Widget content;
  final String? title;
  final AppBar? appBar;
  final Widget? drawer;

  DefaultScreen({required this.content, this.title, this.drawer, this.appBar});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: title != null
          ? AppBar(
              title: Text(title ?? ''),
              centerTitle: true,
            )
          : appBar,
      body: SafeArea(child: content),
      drawer: drawer,
    );
  }
}

class HomeScreen extends StatelessWidget {
  final TextEditingController textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    SearchBar searchBar = SearchBar(
      title: 'Home',
      textController: textController,
    );
    SearchController searchController = searchBar.controller;
    return DefaultScreen(
      appBar: AppBar(
        title: searchBar,
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () {
                if (textController.text.isEmpty)
                  searchController.press();
                else {
                  Navigator.push(context, MaterialPageRoute(builder: (c) {
                    return DefaultScreen(
                      content: SearchResultView(
                        textController.text,
                      ),
                      title: textController.text,
                    );
                  }));
                }
              },
              icon: Icon(Icons.search))
        ],
      ),
      content: ShowWidgetsAPI(HomeTVWidget.getTop250TVs()),
      drawer: Drawer(
        child: ListView(
          children: [
            Container(
              padding: EdgeInsets.all(size.width*0.05),
              width: double.infinity,
              height: size.height * .2,
              child: FittedBox(
                child: Row(children:  [
                  Icon(Icons.play_circle_fill),
                  SizedBox(width: size.width*0.025,),
                  Text('My Movies'),
                ]),
              ),

              color: Theme.of(context).primaryColor,
            ),
            Card(
              child: ListTile(
                title: Text('About'),
                leading: Icon(Icons.info_outline),
                onTap: (){
                  showAboutDialog(
                      context: context,
                      applicationIcon: Icon(Icons.play_circle_fill),
                      applicationName: 'My Movies',
                      applicationVersion: '1.0.0',
                      applicationLegalese: 'ITE-Ameer © 2021',
                      children: [
                        Text('this App created in flutter course'),
                        Text('By Ameer Abo AlShar'),
                        Text('email : ameer.mamoon@gmail.com'),
                        Text('phone : +963 934 801 138')
                      ]
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ShowInfoScreen extends StatelessWidget {
  final tvModel model;

  ShowInfoScreen(this.model);

  @override
  Widget build(BuildContext context) {
    return DefaultScreen(
      title: model.title,
      content: OrientationBuilder(builder: (context, orientation) {
        List<Widget> content = [
          Expanded(
            flex: 9,
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fitWidth,
                    alignment: Alignment.center,
                    image: NetworkImage(model.image)),
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: ShowInfoWidget(model),
          )
        ];
        return orientation == Orientation.portrait
            ? Column(
                children: content,
              )
            : Row(
                children: content,
              );
      }),
    );
  }
}
