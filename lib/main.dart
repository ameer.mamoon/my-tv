import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:my_tv/Screens/default.dart';



void main() => runApp(const MyApp());


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      theme: ThemeData.light().copyWith(
        scaffoldBackgroundColor: Color(0xFFF4F4F4),
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.blueGrey
        ),
         primaryColor:  Colors.blueGrey
      ),

      darkTheme: ThemeData.dark().copyWith(

      ),
       home: HomeScreen(),
    );
  }
}
